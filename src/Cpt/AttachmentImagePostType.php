<?php

namespace Wpify\Core\Cpt;

use Wpify\Core\Models\AttachmentImageModel;

class AttachmentImagePostType extends AttachmentPostType {
	/**
	 * @inheritDoc
	 */
	public function model(): string {
		return AttachmentImageModel::class;
	}
}
