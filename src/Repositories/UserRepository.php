<?php

namespace Wpify\Core\Repositories;

use Doctrine\Common\Collections\ArrayCollection;
use Wpify\Core\Abstracts\AbstractComponent;
use Wpify\Core\Interfaces\RepositoryInterface;
use Wpify\Core\Models\UserModel;

class UserRepository extends AbstractComponent implements RepositoryInterface {
	public function all(): ArrayCollection {
		$collection = new ArrayCollection();
		$users      = get_users();

		foreach ( $users as $user ) {
			$collection->add( $this->get( $user ) );
		}

		return $collection;
	}

	public function get( $user ): UserModel {
		$model = $this->plugin->create_component( UserModel::class, ['user' => $user] );
		$model->init();

		return $model;
	}

	public function get_current_user() {
		$user = wp_get_current_user();

		return $this->get( $user );
	}
}
