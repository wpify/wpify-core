<?php

namespace Wpify\Core\Abstracts;

use WP_Widget;
use Wpify\Core\Traits\ComponentTrait;

abstract class AbstractWidget extends WP_Widget {
	use ComponentTrait;
}
