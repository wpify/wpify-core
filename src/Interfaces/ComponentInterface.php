<?php

namespace Wpify\Core\Interfaces;

interface ComponentInterface {
	/** @return bool */
	public function setup();
}
