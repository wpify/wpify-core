<?php

namespace Wpify\Core\Models;

use WC_Order_Item_Product;
use Wpify\Core\Abstracts\AbstractWooOrderItemModel;

/**
 * @package Wpify\Core
 * @property WC_Order_Item_Product $wc_order_item
 */
class WooOrderItemModel extends AbstractWooOrderItemModel {

}
