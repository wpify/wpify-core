<?php

namespace Wpify\Core\Models;

use Wpify\Core\Abstracts\AbstractPostTypeModel;

/**
 * @package Wpify\Core
 */
class PostTypeModel extends AbstractPostTypeModel {
}
